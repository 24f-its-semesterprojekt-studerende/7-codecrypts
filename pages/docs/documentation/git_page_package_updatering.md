# Git page package updating  

### I dette eksempel vises hvordan man kan updatere sin git page med nye packets. i denne documentation bliver der installeret et nyt theme til Mkdocs.

OBS. denne guide er udført på Windows, men fremgangsmåden er den samme på Linux bare med en anden syntax
---
1. Start med at updatere det lokale repository, dette gøres med en git kommando i roden af dit repository:

```
git pull
```

2. Aktiver et virtuelt environment i roden af repositoriet for at sikkre dig at pakken bliver installeret i environmentet og ikke på din lokale pc

```
.venv\Scripts\activate
```
* Hvis du ikke har et virtuelt environment så kan du følge denne guide: https://gitlab.com/24f-its-semesterprojekt-studerende/7-codecrypts/-/blob/Development/README.md?ref_type=heads

3. Installer pakken med pip, i dette eksempel installere jeg en ny theme til min Mkdocs side

```
pip install mkdocs-dracula-theme
```

4. Naviger til siten og åben filen:

```
pages\mkdocs.yml
```

5. I mkdocs.yml skal theme navnet ændres til navnet på den nye theme, i dette tilfælde er det drcula

```
theme:
  name: dracula
```

6. Test om installationen lykkes lokalt vad at navigere til pages og lav en test side:

```
mkdocs serve
```
7. Hvis alt ser ud som det skal, så skal requrements.txt filen nu updateres for at sikkre at git CI/CD pipelines installere de rigtige pakker under jobbet på gitlab eller github. Det gøres ved denne kommando:

```
pip freeze > requirements.txt
```

8. Nu kan repositoriet skubbes updateres:

```
git add .
```
```
git commit -m "min commit besked"
```
```
git push
```


