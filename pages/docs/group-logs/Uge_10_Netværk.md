# Uge 10 - Netværk

I denne uge har vi arbejdet med [ugens opgaver](https://ucl-pba-its.gitlab.io/24f-its-nwsec/weekly/ww_10/)

## Opgave 25 - OPNsense lokal overvågning

Vi har fulgt denne guide til at opsætte lokal overvågning på vores OPNsense på vores proxmox:

[Link til opgaven](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/25_opnsense_local_logging/)

Selve guiden er meget beskrivende, og billederne deri giver et godt indblik, eftersom vi ingen trafik på vores proxmox/OPNsense har.

## Opgave 54 - Implementering af netværksdesign

Vi har fulgt denne guide til at opsætte lokal overvågning på vores OPNsense på vores proxmox:

[Link til opgaven](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/54_network_design_implementation/)

### Opgave 1 - OPNsense VM opsætning

![Interfaces af netværk](../images/Uge_10/Netværk/Netværk_Interfaces.png)

### Opgave 2 - Kali netværksopsætning

Efter at have sat vores kali maskine op som vi havde fulgt i guiden, skulle vi køre et NMap scan mod vores forskellige netværk.

Vores første scan blev mod vores management netværk:

`nmap -sS -vv 10.37.10.1`

`-sS` er Stealth Scan som er default og den mest populære scan. Man kan læse mere om dette scan https://nmap.org/book/synscan.html

`-vv` er at gøre den "very verbose".

Vores output blev således:

![NMap Scan af management netværk](../images/Uge_10/Netværk/NMapScan_Management.png)

Derefter kørte vi vores næste NMap scan på vores DMZ:

`nmap -sS -vv 10.37.20.1`

![NMap Scan af DMZ](../images/Uge_10/Netværk/NMapScan_DMZ.png)
