# Uge 10 - Fysisk sikkerhed og Host-based firewall

[Link til ugens opgaver](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/weekly/uge_10/)

## Fysisk sikkerhed i forbindelse med CIA

- Ødelagt computer.
  - Her bliver A i CIA brudt, fordi vi ingen adgang har til computeren. Det kan hjælpes med redundans i nogle scenarier.
- Stjålet computer.
  - Her bliver C, I og A i CIA brudt, fordi man kan ikke garantere nogle af tingene, fordi man kan ikke love dens fortrolig data, integritet eller fortrolighed.
- trussel aktør har fået fysisk adgang til en computer uden at ødelægge eller stjæle den.
  - Afhænger af hvor mange rettigheder trussel aktøren har fået, men hvis man er uheldig, så er hele CIA brudt.

## Uge 10 - iptables i Linux

Opsætning af iptables udført ved hjælp af denne guide.

[Link til opgaven her](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/19_Linux_Installing_iptables/)

- Hvad menes der med firewall regler samt hvad menes der med "regelkæden"?

  - Firewall regler er de regler vi opsætter på vores maskiner, i forhold til den netværks trafik der kommer ind og ud på maskinen.
  - Regelkæden går fra toppen til bunden.

Her er et billede af firewall regler.

![Firewall regler](../images/Uge_10/System_sikkerhed/Firewall_regler.png)

**Input** er det der kommer ind i vores maskine.

**Forward** er når der skal kommunikeres fra vores maskine og hen til en maskine.

**Output** er den udadgående kommunikation fra maskinen.

## Uge 10 - Nægt alt netværks trafik i IP Tables

Nægt alt netværks trafik i IP tables er blevet udført ved hjælp af denne guide.

[Link til opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/20_Linux_Dissallowing_everything_iptables/)

## Uge 10 - Opsætning af Wazuh Server

Vi har fulgt denne opsætnings guide: [Link til opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/25_1_Setting_up_Wazuh/)

Som minimum er der krav på at gøre brug af disse porte og protokoller af Wazuh

![Wazuh protokoller og porte](../images/Uge_10/System_sikkerhed/Wazuh_Porte_Protokoller.png)

- Overvej om alle brugerne der anvendes er hensigtsmæssige?
  ![alt text](../images/Uge_10/System_sikkerhed/Wazuh_Services.png) - I denne situation er det hensigtsmæssigt, at de processer som kører Wazuh, er både Root og brugeren Wazuh samt CodeCrypts der kører processerne.
