# Uge 12 - Netværks og kommunikations sikkerhed

[Link til ugens opgaver](https://ucl-pba-its.gitlab.io/24f-its-nwsec/weekly/ww_12/#ugens-emner-er)

## Øvelse 42 - Enumering med offensive værktøjer

[Link til opgaverne](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/42_enumeration/)

Her skal vi gøre brug af offensive værktøjer på Kali fra vores Proxmox, samtidig med at vi skal gøre brug af vores OPNSense til at se vores trafik.

Her er en lille liste af vores web discovery for at finde URL.

![alt text](../images/Uge_12/Netværk/Ffuf_list.png)

Det er nævneværdigt der er config, docs, external samt en gitignore, fordi status koden 200 (OK) betyder at det kan tilgåes, hvorimod 403 betyder "Forbidden" som betyder man ikke har den korrekte autorisering.

Derefter er det muligt at se i OPNsense hvor alt trafikken her:

Firewall -> Log Files -> Live View

![alt text](../images/Uge_12/Netværk/Firewall_Live_View.png)

Her er det muligt at se den trafik der sker.

![alt text](../images/Uge_12/Netværk/Firewall_Log_File_Live_View.png)
