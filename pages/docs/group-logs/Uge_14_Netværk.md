# Uge 12 - Netværks og kommunikations sikkerhed

[Link til ugens opgaver](https://ucl-pba-its.gitlab.io/24f-its-nwsec/weekly/ww_14/)

## Øvelse 26 - IDS/IPS viden

Spørgsmål som skal besvares:

1. **Forklar med egne ord hvad et intrusion detection system gør, underbyg med troværdige kilder (links)**

   - Er software eller en enhed som monitorere et netværk eller et system og kigger på ondsindet aktivitet og opsatte regler. [Link til kilde](https://en.wikipedia.org/wiki/Intrusion_detection_system)

2. **Forklar med egne ord hvad et intrusion prevention system gør, underbyg med troværdige kilder (links)**

   - Er software eller en enhed som har muligheden for at svare igen på den ondsindet aktivitet og de opsatte regler som folk prøver at bryde. Kan afbryde/blokerer noget, kan også åbne op for det igen (Hvis det nu ikke var sket, eksempelvis falsk positiv.) [Link til kilde](https://en.wikipedia.org/wiki/Intrusion_detection_system)

3. **Forklar forskellen på et Netværks IDS/IPS (NIDS) og et host IDS/IPS (HIDS)**

   - Network Intrusion Detection System (NIDS) er et system som analyserer indadgående netværks trafik. Hvorimod Host Intrusion Detection System (HIDS) er et system som monitorere sin host maskine. [Link til kilde](https://en.wikipedia.org/wiki/Intrusion_detection_system)

4. **Forklar med egne ord hvad signatur baseret detektering er, underbyg med troværdige kilder (links)**

   - Kigger efter specifikke mønstre, og derefter en alarm ud fra hvad der sker. [Link til kilde](https://en.wikipedia.org/wiki/Intrusion_detection_system)

5. **Forklar med egne ord hvad anomali baseret detektering er, underbyg med troværdige kilder (links)**

   - Anomali baseret er til at kunne detektere ukendte angreb, her kan man eksempelvis gøre brug af Machine Learning til at kunne detektere med. [Link til kilde](https://en.wikipedia.org/wiki/Intrusion_detection_system)

6. **Forklar mulige problemer med hhv. signatur og anomali baseret detektering**
   - [Link til vores kilde](https://www.linkedin.com/advice/0/what-pros-cons-signature-based-vs-anomaly-based)
   - Signatur kan ikke detektere zero day, og andre ikke så kendte angreb.
   - Anomali kan være svært at opretholde hvad en "normal" opførsel er især for komplekse og store netværk. Kan give stor rate af falsk negativ samt angreb som går lige forbi ens system.

## Øvelse 27 - Suricata opsætning

Her skulle vi opsætte Suricata på vores egen VM. Derefter skulle vi besvare nogle spørgsmål.

- Fast.log kigger på vores regel og ser hvad der er opsamlet.
- Suricat.log er selve suricats log som selve programmet giver os tilbage, når vi vil gemme/køre vores suricat.
- stats.log er nogle statistik log over hvilke ting der bliver brugt.
- eve.json er ligesom Fast.log.
