# Uge 14 - Audit & efterforskningsproces

[Link til ugen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/weekly/uge_14/)

# Efterforsknings proces

Her skulle vi snakke om hvordan vi ville efterforske en "sag".

Vi har valgt at gøre brug af Martins efterforsknings proces.

![Martins efterforskning](../images/Efterforskning.png)

Vi har en host og en server.

### Hændelse

- Der er bruger som har superuser rettigheder, som er blevet detekteret af auditd.

### Identificering

- Ved hjælp af auditd, kan vi se hvem der har ændret rettigheder, hvad der er præcist sket, samt hvorhenne. Ved hjælp af dette, kan vi følge sporet/sporene.

### Isolering af systemet / Beslaglæggelse

- Sluk for host og derefter afskærme systemet

### Opret kopi af det originale udgangspunkt / Imaging

- Opret en kopi af diverse logs og backups.

### Sikre integriteten / Hashing

- Man kan kigge på filernes hash sum, og se om den er ændret.

### Analyser

- Læse vores logs og backups

### Rapporter

- Vi rapporter samt dokumentere hændelsen, så andre kan lære om det.
