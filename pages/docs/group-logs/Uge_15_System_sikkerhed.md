## Uge 15 - Intro til SIEM

[Link til ugen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/weekly/uge_15/)


### Intro til SIEM - Powerpoint opgave

Vi fik til opgave at snakke om hvordan vi ville designe et SIEM system.

![Intro til SIEM opgaver](../images/Uge_15/System_sikkerhed/Opgave_Intro_Til_SIEM.png)

- Hvilke komponenter skal der indgå?
    - Først ville vi have et IDS (intrusion detection system) som har til opgave at detektere de ting der sker på et given system.
    - Derefter ville vi implementere et SIEM (Security Information and Event Management), som har til opgave at oprette logs, og underrette ved enten mail/SMS og give en advarsel i SIEM, hvis der sker brud på regler.
    - Så ville vi have IPS (intrusion prevention system) som kan lukke brugere fra vores system, som laver brud på reglerne.
