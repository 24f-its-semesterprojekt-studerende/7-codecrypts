# Uge 16 - Trådløs sikkerhed

[Link til ugens opgaver](https://ucl-pba-its.gitlab.io/24f-its-nwsec/weekly/ww_16/)

---

## Øvelse 100 - Trådløs viden

[Link til opgaverne](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/100_wireless_viden/)

I denne øvelse skal vi undersøge og besvare nogle spørgsmål.

1. Hvad er 802.11? Forklar med egene ord.
    - Det er en samling af standarder med regler som specificere hvilke protokoller der skal bruges for at implementere wireless local area network.
2. Hvor mange standarder findes der pt. i 802.11 familien?
    - 21 standarder findes der i 802.11 familien.
3. Hvad er Wi-Fi?
    - Wi-Fi er radio bølger som vil kommunikerer over et trådløs netværk.
4. Hvilke frekvenser benyttes?
    - 2.4GHz og 5GHz og meget snart 6.
5. Hvad er kanaler?
    - Hvis der er mange routere på samme frekvens i området. Så bliver routeren nød til at kommunikerer på en anden kanal, for at kunne være på samme frekvens.
6. Hvor mange MB svarer 1000 Mb til? (hvad er formlen til at omregne?)
    - For at beregne fra Mb til MB eller fra Mb til MB, kan man beregne det på 2 måder. 8 bit svarer til 1 byte.
    - MB = 1000 Mb / 8.
    - Mb = MB * 8.

## Øvelse 101 - Trådløs sikkerhed viden

[Link til opgaverne](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/101_wireless_sikkerhed_viden/)

I denne øvelse skal vi undersøge og besvare nogle spørgsmål.

1. Hvad er et SSID?
    - Service Set Identifier (SSID) er navnet på ens trådløse netværk. Trådløse routere eller access points udsender SSID'er så enheder i enheder kan finde og vise alle tilgængelige netværk.

2. Hvad er WEP, WPA, WPA2 og WPA3? Regnes de alle for sikre?
    - **WEP** (Wired Equivalent Privacy) blev introduceret i 1997, og har sikkerheds fejl. Alt trafikken bruger en nøgle, som er statisk som kan bruges til at forbinde enheder på en trådløst netværk.
    - **WPA** (Wi-Fi protected Access) skulle erstatte WEP i 2003. I stedet for at give alle brugere med samme nøgle, bruger den en temporal key integral protocol (TKIP) som dynamisk ændre nøglen. Nu kan trusselsaktører ikke sammenligne den statiske nøgle med WEP, da nøglen nu er mere dynamisk og ændrer sig.
    - **WPA2** er version 2 af WPA. Gør brug af 2 måder. Personlig og pre-shared key (WPA2-PSK) bruger en delt kode for at kunne få adgang. WPA2 PSK bruges primært til hjemme brug. Hvorimod WPA2-EAP er brugt til organisationer.
    Begge måder af WPA2 bruger Counter Mode Cipher Block Chaining Message Authentication (CCMP) som gør brug af den advanceret encryption standard (AES).
    (Advanced Encryption Standard)
    - **WPA3** sammensætter både autentificering og kryptering samlet, som sørger for at integriteten og fortrolighed i Wi-Fi kommunikationen.
    - **HVILKE ER SIKRE OG HVILKE ER IKKE** den mest sikre er WPA3, derefter er det WPA2, WEP og WPA er de mindst sikre.

3. Hvad er forskellen på PSK og IEE 802.1X/EAP autentificering?
    - Er forskellen PSK en delt nøgle, og EAP er enterprise hvor hver eneste er har en nøgle for sig selv. Det er en måde man autentificerer sig op på netværket.

4. Hvad er formålet med Wi-Fi 4 way handshake? Kan det misbruges? Hvis ja, hvordan?
    - Skal en Wi-Fi forbindelse mellem en enhed og et access point. Der bliver delt PSK, MAC addresse og andet. Så det gør at vores forbindelse bliver krypteret mellem Access point og enhed. Kan det misbruges? Ja, fordi det kan opfanges, og så kan man brute force oplysningerne (krypterings nøglen).

5. Hvad er et PMKID interception attack? Beskriv med egne ord hvordan det fungerer
    - Pairwise Master Key Identifier er et angreb på PSK, og derefter kan man lave et bruteforce angreb. 

6. Hvad er en lavpraktisk måde at beskytte netværk med PSK?
    - Lave et tilfældigt og langt adgangskode. Det kunne være tilfældige bogstaver, tal og tegn og så langt så muligt.

## Øvelse 103 - Trådløs CTF

Vi har valgt at gøre brug af airgeddon til at opsamle informationer på routeren.

Vi valgte at gøre brug af Handshake/PKMID tools ved at trykke på 5.

![alt text](../images/Uge_16/Netværk_CTF/image-5.png)

Derefter valgte vi at gå ind i monitor mode (2).

Derefter trykkede vi på 6, for at gå ind og kigge efter handshakes.

![alt text](../images/Uge_16/Netværk_CTF/image.png)

Her valgte vi så derefter netværket ved at skrive 95.

![alt text](../images/Uge_16/Netværk_CTF/image-1.png)

Derefter kan vi vælge hvor vi ville gemme det handshake.

![alt text](../images/Uge_16/Netværk_CTF/image-2.png)

Her valgte vi at gå ind og crack adgangskoden igennem airgeddon. Som gøres ved at være i hovedmenuen, og trykke på 6 for at decrypt.

![alt text](../images/Uge_16/Netværk_CTF/image-6.png)

Derefter trykker man 1, for at gå til personal, fordi vi ved at det er en personlig router.

![alt text](../images/Uge_16/Netværk_CTF/image-7.png)

Derefter trykker vi 1, for at kunne lave et dictionary attack mod den capture file vi fik igennem vores handshake.

![alt text](../images/Uge_16/Netværk_CTF/image-8.png)

Her er den så igang med at lave et dictionary attack.

![alt text](../images/Uge_16/Netværk_CTF/image-3.png)

Her fik vi adgangskoden til routeren.

![alt text](../images/Uge_16/Netværk_CTF/image-4.png)

Vi fandt gateway ved at kigge på skærmen som der blev vist. Derefter tilgik vi den.

![alt text](../images/Uge_16/Netværk_CTF/image-9.png)

Brugernavn var Root - adgangskoden var Netcompany123

![alt text](../images/Uge_16/Netværk_CTF/image-10.png)

Og her kan vi nu se at netværket hedder nu CodeCrypts

![alt text](../images/Uge_16/Netværk_CTF/image-11.png)