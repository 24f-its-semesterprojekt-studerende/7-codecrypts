---
hide:
#  - footer
---

# Læringslog uge 05 - **Introduktions uge**

## Emner

Ugens emner er:

- Opsætning af server
  - RAM tilkobling
  - Harddrive udskiftning
  - Tilpassende og tilstrækkelig vold
- Proxmox

## Mål for ugen

### Praktiske mål

**Praktiske opgaver vi har udført**

- Proxmox

# Gennemgang

PC/Motherboard med cover

![PC/Motherboard med cover](../images/Uge_5/Computer.png)

RAM uden cover

![RAM uden cover](../images/Uge_5/RAM.png)

Harddisk ikke taget ud endnu

![Harddisk ikke taget ud endnu](../images/Uge_5/Harddisk.png)

System information

![System information](../images/Uge_5/SystemInformation1.png)

System information

![System information](../images/Uge_5/SystemInformation2.png)

Proxmox installation

![Proxmox installation](../images/Uge_5/ProxmoxOversigt.png)
