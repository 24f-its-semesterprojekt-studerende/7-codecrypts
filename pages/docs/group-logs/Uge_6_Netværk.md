---
hide:
#  - footer
---

# Læringslog uge 06 - Netværk

[Her er et link til ugens opgaver samt hvad vi skal lære denne uge.](https://ucl-pba-its.gitlab.io/24f-its-nwsec/weekly/ww_06/)

## Emner

Ugens emner er:

- VMWare
- OPNsense
- Proxmox
- Kali
- Grundlæggende netværksteori

## Mål for ugen

- Opsætning af OPNsense på henholdsvis VMWare og Proxmox.

### Læringsmål

**Læringsmål vi har arbejdet med**

Den studerende har viden om og forståelse for

- **Viden:**
  - Hvilke enheder, der anvender hvilke protokoller
  - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

## Reflektioner over hvad vi har lært

- Forventnings afstemt hvad faget handler om.

# Opgaver

## Opgave 1

[Link til opgaven](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/00_learning_goals/)

Målet med opgaven er at vi skal finde læringsmålene i studieordningen for faget, og derefter debater hvad læringsmålene betyder, samt hvad vi kan bruge dem til.

### Viden

Den studerende har viden om og forståelse for:

- **Netværkstrusler**

  - Hvilke trusler der skal være på et netværk.

- **Trådløs sikkerhed**

  - Usikkerheder ved wifi og andre typer for trådløs forbindelser.

- **Sikkerhed i TCP/IP**

  - Lære om de forskellige lag i TCP/IP, samt hvor vi skal sikre usikkerhederne.

- **Adressering i de forskellige lag**

  - Lære om de forskellige lag i OSI og TCP/IP.

- **Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl)**

  - Få kendskab til internet protokoller såsom

- **Hvilke enheder, der anvender hvilke protokoller**

  - Vi synes denne er meget uklar, så vi er ikke 100% sikker hvad der menes med denne.

- **Forskellige sniffing strategier og teknikkerv**

  - Man kan se hvor de forskellige adresser og trafik går hen.

- **Netværk management (overvågning/logning, snmp)**

  - Vi lærer hvordan man laver overvågning samt logger de ting der foregår på netværket.

- **Forskellige VPN setups**

  - Hvordan vi sætter en VPN op

- **Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).**

  - Hvordan man benytter forskellige netværks procedurer til at beskytte et netværk.

### Færdigheder

Den studerende kan:

- **Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)**

  - Man kan overvåge et netværk ved hjælp af diverse netværkskomponenter såsom IDS (Intrusion Detection System) og IPS (Intrusion Prevention System) som er med til at beskytte. HoneyPot er et sårbart system hvor man kan arbejde på det.

- **Teste netværk for angreb rettet mod de mest anvendte protokoller**

  - Angreb man kan udsætte netværk imod såsom DDoS.

- **Identificere sårbarheder som et netværk kan have.**

  - Anset som meget vigtig af studiegruppen (Specielt Emil).

### Kompetencer

Den studerende kan håndtere udviklingsorienterede situationer herunder:

- **Designe, konstruere og implementere samt teste et sikkert netværk**

  - Vi har mulighed for at designe, konstruere og implementere et netværk, så vi kan impkementere et sikkert netværk.

- **Monitorere og administrere et netværks komponenter**

  - Vi er ikke 100% sikre.

- **Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)**

  - At vi kan lave en rapport over sårbarheder på et netværk.

- **Opsætte og konfigurere et IDS eller IPS.**

  - Dette læringsmål er vi enige om at dette gav mening.

# Opgave 2

[Link til opgaven](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/10_basic_network_knowledge/)

I denne opgave skal vi besvar følgende spørgsmål:

- **Hvad betyder LAN?**

  - Local Area Network - Vores netværk.

- **Hvad betyder WAN?**

  - Wide Area Network - Er noget som strækker sig over større geografiske områder.

- **Hvor mange bits er en ipv4 adresse?**

  - 32 bit fordi xxx.xxx.xxx.xxx

- **Hvor mange forskellige ipv4 adresser findes der?**

  - 4,294,967,296 - 2^32.

- **Hvor mange bits er en ipv6 adresse?**

  - Er 128 bits og indholder 8 16 bit felter (xxxx.xxxx.xxxx.xxxx.0000.0000.0000.0000)

- **Hvad er en subnet maske?**

  - En måde at opdele et netværk op på, til hvor meget der går til netværket og hvor meget der går til en host.

- **Hvor mange hosts kan der være på netværket 10.10.10.0/24**

  - 8 bits går til host. Det vil sige 254 er tilgængelige på netværket.

- **Hvor mange hosts kan der være på netværket 10.10.10.0/22**

  - 10 bits går til host. Det vil sige 1022 er tilgængelige på netværket.

- **Hvor mange hosts kan der være på netværket 10.10.10.0/30**

  - 2 bits går til host. Det vil sige 2 er tilgængelige på netværket.

- **Hvad er en MAC adresse?**

  - Er en fysisk adresse på et netværksinterface.

- **Hvor mange bits er en MAC adresse?**

  - 48 bits.

- **Hvilken MAC adresse har din computers NIC?**

  - Man kan gennem CMD skrive IPConfig /all.

  ![alt text](../images/Uge_6/MACAddresse.png)
  0-9+A-F

- **Hvor mange lag har OSI modellen ?**

  - 7 lag

  ![alt text](../images/Uge_6/OSI.png)

- **Hvilket lag i OSI modellen hører en netværkshub til?**

  - Lag 1

- **Hvilket lag i OSI modellen hører en switch til?**

  - Lag 2

- **Hvilket lag i OSI modellen hører en router til?**

  - Lag 3

- **Hvilken addressering anvender en switch?**

  - MAC Addressering

- **Hvilken addressering anvender en router?**

  - IP

- **På hvilket lag i OSI modellen hører protokollerne TCP og UDP til?**

  - Lag 4 - Transport laget.

- **Hvad udveksles i starten af en TCP forbindelse?**

  - SYN - Her gøres brug af 3 way handshake.

- **Hvilken port er standard for SSH?**

  - 22

- **Hvilken port er standard for https?**

  - 443

- **Hvilken protokol hører port 53 til?**

  - TCP/UDP - DNS

- **Hvilken port kommunikerer OpenVPN på?**

  - UDP Port 1194 og TCP Port 443

- **Er FTP krypteret?**

  - Er ikke krypteret.

- **Hvad gør en DHCP server/service?**

  - Uddeler IP adresser indenfor nogle parametere på netværket.

- **Hvad gør DNS?**

  - DNS = Domain name system oversætter IP adresser til domæner. Eksempelvis skriver man google.com kommer man på 8.8.8.8

- **Hvad gør NAT?**

  - Oversætter din lokale IP addresse til en offentlig addresse, som kommunikerer udadtil.

- **Hvad er en VPN?**

  - Virtual Private Network er et privat netværk som kan tilgås.

- **Hvilke frekvenser er standard i WIFI?**

  - 2,4GHz og 5GHz

- **Hvad gør en netværksfirewall?**

  - Blokere og tilader trafikken på netværket.

- **Hvad er OPNsense?**

  - Er et open source firewall og router platform. Netværks software, som også indeholder IDS og IPS.

# Opgave 3

Installation af proxmox på VMWare, har vi udført ved at følge denne dokumentation: https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/03_opsense_vmware/

# Opgave 3B

Installation af OPNsense på proxmox på vores PC i netlab, har vi udført ved at følge denne dokumentation: https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/03b_opnsense_proxmox/

# Opgave 3C

På nuværende tidspunkt er det ikke muligt at finde svarene på disse spørgsmål, da vores server er gået ned.
