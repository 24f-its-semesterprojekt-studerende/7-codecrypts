---
 hide:
#  - footer
---

# Læringslog uge xx - *tekst*

## Emner

Ugens emner er: 

- Introduktion til faget "System sikkerhed"
- Opsætning af ubuntu server

## Mål for ugen

- I denne uge er målet at de studerende forstår hvad de forventes af dem i faget, og hvad de overordnet læringsmål for faget er.

### Praktiske mål

- Alle studerende har forståelse for fagets formål.
- Alle studerende kan (Nogenlunde)tolke læringsmålene for faget.
- Alle studerende har en fungerende Ubuntu Server VM.

**Praktiske opgaver vi har udført**

## Installation og opsætning af VirtualBox
Vi gjorde brug af denne dokumentation for at installere samt opsætte VirtualBox. https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/1_1_Ops%C3%A6tning_virtual_box/#information

## Opsætning af Ubuntu Server
Vi gjorde brug af denne dokumentation for at installere samt opsætte VirtualBox. https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/1_2_Ops%C3%A6tning_ubuntu_Server/#information

## Opsætning af Kali Linux
Vi gjorde brug af denne dokumentation for at installere samt opsætte Kali Linux. https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/1_3_Ops%C3%A6tning_Kali_Linux/#information

## Validering af netværks forbindelse
Vi gjorde brug af denne dokumentation for at validerer vores netværks forbindelse. https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/1_4_Validere_forbindelse_mellem_Kali_Og_Ubuntu_Server/