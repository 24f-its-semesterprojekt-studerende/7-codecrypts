# Uge 07 - Læringslog - OSI modellen og netværks protokoller

[Her er et link til ugens opgaver samt hvad vi skal lære denne uge.](https://ucl-pba-its.gitlab.io/24f-its-nwsec/weekly/ww_07/)

# Øvelse 11 - OSI Modellen

[Link til øvelsen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/11_osi_model/)

![Billede af OSI modellen](../images/Uge_7/Netværk/OSI_modellen.png)

Vi nåede primært at få kigget på OSI modellen, og derefter havde en god debat omkring hvad og hvordan OSI modellen er.

**_Lag 7_**

- Er applikations laget, som er de applikationer der kommunikere på netværket, så f.eks. DNS eller HTTP.

**_Lag 6_**

- Er præsentations laget, som handler om formatering af data samt kryptering.

**_Lag 5_**

- Er sessions laget, som er det der skaber forbindelsen mellem de to maskiner der skal kommunikerer.

**_Lag 4_**

- Er transport laget, hvor der bliver en kommunikations protokol, og der bliver kommunikeret ud fra disse.

**_UDP_**

Dette er et eksempel på hvordan UDP kommunikerer.

![UDP image](../images/Uge_7/Netværk/UDP_Picture.png)

Her er et skema over fordele og ulemper af UDP.

![Fordele og ulemper ved UDP](../images/Uge_7/Netværk/UDP_Advantages_Disadvantages.png)

**_TCP_**

Dette er et eksempel på hvordan TCP kommunikerer.

![TCP image](../images/Uge_7/Netværk/TCP_Picture.png)

Her er et skema over fordele og ulemper af TCP.
![alt text](../images/Uge_7/Netværk/TCP_Advantages_Disadvantages.png)

**_Lag 3 - Network_**

- Er her hvor routing sker, som sker ved hjælp OSPF (Open Shortest Path First), hvor den finder den korteste vej til destinationen.

  - Et eksempel når dette sker er ved hjælp af traceroute.

**_Lag 2 - Data link_**

- Er det lag, hvor man sætter den fysiske addresse (MAC addresse) på det der skal sendes. Den fysiske addresse er på de ting som har et netværks kort (NIC - Network interface card).

**_Lag 1 - Physical_**

- er det fysiske lag, hvor man snakker om netværkskabler.

# Øvelse 12

[Link til øvelsen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/12_osi_protocol/)

I denne opgave skulle vi bruge wireshark til at sniffe og analysere den trafik vi modtog.

Vi søgte på Google efter "HTTP Websites", hvori der var en side som hed neverssl.
Derefter gik vi på den side, så derfor har vi en mulighed for at kunne se den trafik der foregår på en HTTP side.

For at se de forskellige netværks protokoller der er gjort brug af som er i lag 7 og lag 6.

![Billede over de forskellige protokoller](../images/Uge_7/Netværk/Protocol_Image.png)

Grunden til at vi valgte en HTTP side, var for at kunne finde "line-based text data", for at vise at man ikke får en krypteret tekst.

![HTTP tekst i Wireshark](../images/Uge_7/Netværk/Line_Based_Text_Data.png)

Vi har ikke som sådan læst siden, men vi blev meget hurtigt opmærksomme på, at den fortæller at der er usikre protokoller.

[Kilde](https://www.guru99.com/wireshark-passwords-sniffer.html)

# Øvelse 23 - OPNsense opgave

[Øvelsen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/23_opnsense_explore_proxmox/)

Her skulle vi gennemgå dokumentationen for OPNSense, og derefter besvare nogle spørgsmål:

1. Hvilke features tilbyder opnsense?

   - Firewall og routing platform.

2. Hvordan kan du kontrollere om din opnsense version har kendte sårbarheder?

   - Åben OPNSense i browser, ved at tilslutte ens OPNsenses IP addresse. Derefter navigere til System -> Firmware -> status -> Run an audit og derefter tryk security.

![OPNsense billede](../images/Uge_7/Netværk/opnsense_firmware_security.png)

3. Hvad er lobby og hvad kan man gøre derfra?

   - Der hvor man starter, der hvor man kan finde ens dashboard, afslutte ens session eller ændre adgangskode. Det her er det første sted man besøger når man åbner OPNsense.

4. Kan man se netflow data direkte i opnsense og i så fald hvor kan man se det?

   - Der findes 2 typer data der kan komme igennem en firewall. Ingress som er det der kommer fra og til firewall. Egress er det der kommer igennem firewall.

[Dokumentation](https://docs.opnsense.org/manual/netflow.html)

5. Hvad kan man se omkring trafik i Reporting?

   - Man kan se den trafik af data der foregår i ens firewall.

6. Hvor oprettes vlan's og hvilken IEEE standard anvendes?

   - Interfaces -> Other Types -> VLans. Der bliver gjort brug af IEEE 802.1q når man opretter VLANs.

[Dokumentation](https://pve.proxmox.com/wiki/Network_Configuration)

7. Er der konfigureret nogle firewall regler for jeres interfaces (LAN og WAN)? Hvis ja, hvilke?

   - Der er ialt 24 regler på LAN som er automatisk genereret.
   - Der er ialt 24 regler på WAN som er automatisk genereret.

8. Hvilke slags VPN understøtter opnsense?

   - OpenVPN - SSL

[Dokumentation](https://docs.opnsense.org/manual/how-tos/sslvpn_client.html)

9. Hvilket IDS indeholder opnsense?

   - Suritica

[IDS](https://docs.opnsense.org/manual/ips.html)

10. Hvor kan man installere os-theme-cicada i opnsense?

    - System -> Firmware -> Plugins -> søg derefter os-theme-cicada.

11. Hvordan opdaterer man firmware på opnsense?

    - System -> Firmware -> Status -> Check for updates.

12. Hvad er nyeste version af opnsense?

    - 24.1.1

# Øvelse 40 - TryHackMe NMap

[Link til øvelsen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/40_thm_nmap/)

I denne opgave, skulle vi arbejde med et TryHackMe rum, og derefter lavede vi et samlede dokument som kunne bruges til at læse hvad NMap er.

[Her er et link til vores side med NMap](../group-logs/Uge_7_Netværk_NMap.md)

# Øvelse 50

[Link til øvelsen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/50_nw_diagram/)

Her var opgaven at vi skulle lave et diagram pænt, som kan ses på linket til øvelsen. Som billedet forneden, kan man se hvordan vores kom til at se ud.

![Netværksdiagram](../images/Uge_7/Netværk/NikolajsDiagram.png)

# Refleksion af denne undervisning

Vi fik en grundlæggende forståelse for TCP/IP og OSI modellen, som vi kunne se pointen i.

Grundlæggende kompetencer for OPNsense og fik en grundlæggende forståelse for wireshark.

Vi synes generelt undervisningen var god.
