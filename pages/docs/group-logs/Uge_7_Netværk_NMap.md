# TryHackMe - NMAP Scan

Vi gjorde brug af dette rum på TryHackMe (THM), som hjalp med at give de grundlæggende kompetencer indenfor NMAP værktøjet.

[TryHackMe - Basic NMap](https://tryhackme.com/room/nmap02)

Vi fik grundlæggende viden om NMap, og hvad forskellen var på de forskellige scans, samt hvad states betød.

Her er et billede af hvad de forskellige states om:

![alt text](../images/Uge_7/TryHackMe/NMap_States.png)


Et eksempel på hvordan three way handshake er i Wireshark taget fra THM.

![alt text](/pages/docs/images/Uge_7/TryHackMe/NMap_Scan_sT.png)


Samt her er et eksempel på en af de scans vi kørte på THM. sS stealth scan som er med TCP.

![NMap - Stealth Scan](/pages/docs/images/Uge_7/TryHackMe/NMap_Stealth_Scan.png)

Sådan ser det ud med -sS i wireshark, som ikke er lige så larmende på netværket.

![alt text](/pages/docs/images/Uge_7/TryHackMe/NMap_Scan_sS.png)