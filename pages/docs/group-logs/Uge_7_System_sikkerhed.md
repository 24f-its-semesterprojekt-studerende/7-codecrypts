# Læringslog uge 07 - *System sikkerhed - Linux*

## Emner

Ugens emner er: 
- Linux

## Mål for ugen

- Grundlæggende Linux

### Praktiske mål

**Praktiske opgaver vi har udført**

- Opgave 3 - Navigation i Linux file system med bash

Opgave vi har udført fra undervisningen:

[Opgaven fra undervisningen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/3_Navigation_i_Linux_filesystem_med_bash/)

- Opgave 4 - Linux file struktur

Opgave vi har udført fra undervisningen:

[Opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/4_Linux_file_struktur/)

Opgave 6 - Søgning i Linux file strukturen

[Opgave 6](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/6_Searching_in_linux_file_system/)

Opgave 7 - Ændring og søgning filer

[Opgave 7](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/7_changing_and_searching_in_files/)


## Opgave 4 - Linux file struktur
![Billede af strukturen i Linux](../images/Uge_7/System_Sikkerhed/Linux_Struktur.png)

/ = Root directory

bin = Binære programmer som kan eventuelt køres.

boot = Er den mappe som indholder boot konfigurationen.

dev = Specielle filer til devices.

etc = Konfigurations filer

home = Personlige mapper (meget frie)

media = Når man indsætter USB, DVD drev og andet, så er filerne inde i denne mappe.

lib = delte biblioteker mellem brugerne på styresystemet.

mnt = Manuel mounting, så ting vi sætter ind i maskinen.

misc = diverse filer og konfigurationer.

proc = Indholder informationer om systemets processer og kernel.

opt = reserveret til installation af add-on applikationer software pakker.

sbin = udlukkende filer kun til sudo.

root = roden af ens pc.

tmp = midlertidige filer

usr = Bliver delt mellem alle brugerne.

var = Er variabel filer, som ændre sig løbende under systemets levetid. Såsom logs, tracking, caches og andre ting.

etc og var er 2 utrolig gode at huske.

## Opgave 6 - Søgning i Linux strukturen

Find kommandoen leder efter i den mappe man står i.

![Find command](../images/Uge_7/System_Sikkerhed/Find_Command.png)

Hvis man skriver find /etc/, så giver alle filerne i etc mappen.

![Find command - etc](../images/Uge_7/System_Sikkerhed/Find_Command_etc.png)

Ved at gøre brug af denne kommando "sudo find /etc/ -name passwd", leder man efter decideret mappen/filen der indholder "passwd".

![Find Command - name passwd](../images/Uge_7/System_Sikkerhed/Find_Command_etc_passwd.png)

Vi prøvede at gøre brug af at søge "sudo find /etc/ -name pasSwd", for at se om den var case sensitive.

![Find -name passWd](../images/Uge_7/System_Sikkerhed/Find_Command_name_pasSwd.png)

Brug - "sudo find /etc/ -iname pasSwd" for at se om det gjorde noget imod case sensitive, og det gjorde det.

![Find -name passWd - incase](../images/Uge_7/System_Sikkerhed/find_command_name_incase_pasSwd.png)

Gør brug af "-name pass*", og så kan vi se hvordan det fungere, som kan kan se så udfylder det fra *.

![Find -name - pass*](../images/Uge_7/System_Sikkerhed/Find_Command_passwd_star.png)

Vi følte ikke dokumentationen var den vigtigste for de næste par opgaver. Dog vil vi hellere beskrive hvad de gjorde.

1. I Home directory, eksekver kommandoen truncate -s 6M filelargerthanfivemegabyte.

    - Her opretter man en fil som indholder 6megabyte.

2. I Home directory, eksekver kommandoen truncate -s 4M filelessthanfivemegabyte.

    - Her opretter vi en fil som indholder 4megabyte.

3. I roden (/), eksekver kommandoen find /home -size +5M.
    - Her skal vi gå til roden "cd /". og derefter bruger find /home -size +5M, så vil man lede efter en fil som indholder 5megabyte eller over.

4. I roden (/), eksekver kommandoen find /home -size -5M.

    - Her skal vi gå til roden "cd /". og derefter bruger find /home -size -5M, så vil man lede efter en fil som indholder 5megabyte eller under.
    
5. I Home directory, opret to directories, et der hedder test og et andet som hedder test2.

    - Her går vi til roden "cd /", og derefter opretter vi både en test og en anden fil test2.

6. I test2, skal der oprettes en fil som hedder test.

    - "cd test2" og "touch test".

7. I Home directory, eksekver kommandoen find -type f -name test.

    - "cd /" og derefter skriv "find -type f -name test", her leder den efter en fil ved navn "test".

8. I Home directory, eksekver kommandoen find -type d -name test.

    - Her leder den efter et directory ved navn test.

# Refleksion

Vi synes undervisningen var god til at opsamle alle i gruppen til forståelse af linux.