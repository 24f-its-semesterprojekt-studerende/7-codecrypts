# Uge 08 - Netværksdesign, segmentering og firewalls

[Planen for dagen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/weekly/ww_08/#skema)

---

# Øvelse 41 - NMap & Wireshark på Proxmox

Her skal lære NMap bedre at kende, samtidig med at vi bruger Wireshark, man kan læse mere om det ved følge [dette link til opgaven](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/41_nmap_wireshark/)

Først køres et NMap Scan til vores netværk 192.168.1.1. Hvor vi kan se at vores netværk har nogle åbne porte, 53 443, 80.

Vi kan se på konsollen hvilke porte der er åbne:

![NMap Scan](../images/Uge_8/Netværk/NMap_Scan_Proxmox.png)

![NMap Scan - Detaljeret](../images/Uge_8/Netværk/NMap_Scan_Proxmox_Detailed.png)

Vi kan også se på wireshark hvordan vores trafik er på vores netværks kort på vores kali linux.

![NMap scan og Wireshark på proxmox](../images/Uge_8/Netværk/NMap_Scan_Wireshark_Proxmox.png)

Nu vil vi prøve at åbne en HTTP server, og derefter vil vi nmap skanne vores netværk.

Her tilgår vi vores server, hvor vi kan se at det er mappen som vi åbnede serveren i.

![Python HTTP Server](../images/Uge_8/Netværk/Python_HTTP_Server.png)

Her kører vi et NMap Scan på vores netværk, for at se hvilke porte der er åbne på netværket.

Hvor vi ser den HTTP Server vi åbnede på port 8000.

![NMap Scan på HTTP Server](../images/Uge_8/Netværk/NMap_Scan_HTTP_Server.png)

![Wireshark på vores NMap Scan](../images/Uge_8/Netværk/NMap_Scan_HTTP_Server_Wireshark.png)

Herefter lukker vi for serveren, og kan se at der ingen forbindelse er.

![NMap Scan uden HTTP Server.](../images/Uge_8/Netværk/NMap_Scan_No_HTTP_Server.png)

---

# Øvelse 51 - Netværksdesign med netværkssegmentering

[Link til opgaven](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/51_nw_segmentation_design/)

Her skulle vi udarbejde et netværksdesign samt segmentering til en opstillet virksomhed, hvor vi skulle diskutere hvordan og hvorfor skulle netværket udarbejdes.

![Netværks diagram](../images/Uge_8/Netværk/Netværks_Diagram_Segmentering_Udkast.png)
