# Uge 08 - CIS Benchmarks

[Link til ugen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/weekly/uge_08/)

## Uge 08 - CIS Kontroller - Opgave 47

[Link til opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/47_CIS_Kontroller/)

I dnene opgave skal vi lave vurdering af en fiktiv virksomhed og deres IT-sikkerhedsmodenhedsniveau i henhold til deres risikoprofil og størrelse.

Vi ser BS Consulting A/S som værende i IG 2, fordi de opbevarer personfølsom data.

Dette svar er opnået ved at kigge på CIS18 dokumentationen delen der omhandler IG 1, 2 og 3.

De skal have rettet i nogle af deres politikker:

- Sikkerhedspolitik som skal opdateres oftere.

- Medarbejdere skal trænes.

- De har en reaktiv tilgang til cybersikkerhed, hvor de burde have en proaktiv.

## Opgave 48 - CIS Benchmarks

[Link til opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/48_CIS_BenchMarks/)

Her skal vi foretage en vurdering af et Ubuntu system overholdelse af udvalgte CIS ubuntu benchmarks sikkerhedsanbefalinger.

Vi lavede 1.6.1.1 som tog lidt tid, men det virkede - vi blev overrasket over hvor simpelt det var at lave en CIS benchmark.

Dog står det frem at en manuel audit ikke er vejen frem, og der nemt kan opstå fejl.

En automatiseret metode virker umiddelbart vejen frem, da det mindsker menneskelige fejl. Da den form for manual arbejde virker mere banalt

# Opgave 49 - Mitre ATT&CK

Her skulle udforske Mitre ATT&CK-rammeværket hvor vi skulle fokusere på taktikken Privilege Escalation, teknikken (T1548), samt afhjælpningen (M1026) og detekteringen (DS0022).

Mitre ATT&CK takikken TA0004 er Privilege Escalation.

Hvor under privilege escalation er der T1548 findes der 5 underteknikker, som hedder .00X fra 1 til 5.

Der er 5 under teknikker til at kunne opnå en "Elevation" i et system. Som hjælper med at få flere tilladelser i et system.

![T1548 Abuse Elevation Control Mechanism](../images/Uge_8/System_Sikkerhed/Mitre_Attack_T1548.png)

Der findes 7 måder man kan mitigere imod en T1548 (Privilege Escalation), hvis man tager højde for M1026 så kan man finde ud af hvordan man kan eventuelt mitigere det.

M1026 - Privileged Account Management - At fjerne den lokale administrator gruppe på systemerne.

Samtidig kan man kigge på hvordan opdager sådan et angreb i sit eget system. En af mulighederne er DS0022 (Data typen er: Fil) og de komponenter som bliver taget i brug er (Fil metadata, modifikation).

Monitorere fil sysstemet for filer som har sat både setuid eller setgid bitene.

Fil modifikation vil man på linux gøre så auditd kan give en advarsel for hver gang en brugers ID og effektive ID (sudo) er forskellig fra det man har som bruger.
