# Uge 09 - Netværk

[Her er et link til ugens opgaver samt hvad vi skal lære denne uge.](https://ucl-pba-its.gitlab.io/24f-its-nwsec/weekly/ww_09/)

## Øvelse 70 - Dokumentation og IPAM

[Link til opgaven](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/70_ipam/)

### Hvad er IPAM?

IPAM (IP address management)

IPAM organisere et kaos.

IPAM er en metode til at organisere IP adresser

- Planlægge
- Overvåge
- Administrere

IPAM kan benyttes isoleret, dog er det en fordel at benytte IPAM i samarbejde med DHCP og DNS.

Fordele ved benytte DNS og DHCP i samarbejde med IPAM:

- Hvilke IP adresser der er tilgængelige.
- Hvilke host names korrelere IP adresser.
- Hvilke enheder er tildelt IP adresserne.
- Subnet brug samt størrelse de er og hvem bruger dem.
- Permanente og midlertidige IP adresser.
- Default routere tildeles til hver netværks enhed.

[Link til udtalelse](https://www.trustradius.com/products/netbox/reviews?qs=pros-and-cons#reviews)

Fordele:

- Enhed modellering
- Enheds inventar
- IPAM

Ulemper:

- GraphQL implementeringen kunne blive bedre integreret.
- Netbox is usually very reactive except but performance start to drop when requesting 250 devices or more per page

Vi tilsluttede os til Netbox, og tilføjede 2 devices, som kan ses nedenunder. Først skulle man tilføje et site, en manfacturer derefter type, og så kunne man tilføje et device til netværket.

![Netbox dokumentation](../images/Uge_9/Netværk/netbox.png)

## Øvelse 24 - OPNsense hærdning

[Link til opgaven](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/24_opnsense_hardening/)

Opgaven gik ud på at vi skulle udføre en hærdning mod en OPNSense instans.

Først gik vi ind i CIS 18 Benchmark for firewalls, og derefter gik vi igang med at udarbejde den.

Vi synes at 6.1 lød spændende, som er "Ensure syslog is configured (Manual)"

![CIS 18 Benchmark Description](../images/Uge_9/Netværk/CIS18_Firewall.png)

![CIS 18 Benchmark Audit](../images/Uge_9/Netværk/CIS18_Firewall_Logging_Audit.png)

For at komme til siden skal man trykke på: System -> Settings -> Logging / Targets. Yderst til højre er der et + som kan man trykke på, for

![OPNSense logging](../images/Uge_9/Netværk/OPNSense_Logging.png)

Og nu har vi tilføjet noget logging

![OPNSense logging tilføjet](../images/Uge_9/Netværk/OPNSense_Logging_Added.png)

Diskutere i gruppen om hvordan I ville kunne holde styr på et større netværk til eksamen

- Gruppen har diskuteret om at opstille et IPAM system som kan holde styr på netværk, IP og et netværksdiagram på et tilstrækkeligt og passende niveau.
