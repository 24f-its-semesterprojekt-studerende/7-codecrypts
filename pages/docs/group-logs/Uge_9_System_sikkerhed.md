# Uge 09 - Linux bruger system & adgangskontrol

[Link til ugen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/weekly/uge_09/)

## Opgave 9 - Bruger kontoer i Linux.

[Link til opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/9_User_accounts/)

Først skal vi oprette en bruger, tildele et password, skift bruger derefter slette en bruger

### Oprettelse af en bruger

![User Add](../images/Uge_9/System_Sikkerhed/Linux_Useradd.png)

### Tildel bruger et kodeord

![User passwd](../images/Uge_9/System_Sikkerhed/Linux_Passwd.png)

### Skift bruger

Har oprettet en bruger med sudo useradd -m Mandolarian3

![](../images/Uge_9/System_Sikkerhed/Linux_Home_ls.png)

su Mandolarian3

![Switch User](../images/Uge_9/System_Sikkerhed/Linux_su.png)

### Slet en bruger

Man skal derefter slette den bruger som man har oprettet med et directory **_Mandolarian_**, og derefter oprette en bruger ved navn **_Ivan_**

Derefter opret en bruger ved navn **_Ivan_**

![Rettigheder i home directory overtaget](../images/Uge_9/System_Sikkerhed/Linux_Home_ls_al.png)

Rettighederne fra den tidligere bruger som man har slettet er blevet overtaget af brugeren Ivan.

## Opgave 10 - Bruger rettigheder i Linux

[Link til opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/10_User_permissions_in_linux/)

Ved at skulle lave rettigheder skal man gøre brug af dette skema som går fra 0-7, det første er en brugeren, gruppen, alle andre.

Så 070 betyder at brugeren/ejeren har ingen rettigheder, gruppen har både skrive, læse og execute rettigheder mens andre ikke har.

| Værdi | Rettighed | Sum   | Betydning                           |
| :---: | :-------- | :---- | :---------------------------------- |
|   0   | ---       | 0+0+0 | Ingen rettigheder                   |
|   1   | --x       | 0+0+1 | Execute rettigheder                 |
|   2   | -w-       | 0+2+0 | Skrive rettigheder                  |
|   3   | -wx       | 0+2+1 | Skrive og execute rettigheder       |
|   4   | r--       | 4+0+0 | Læse rettigehder                    |
|   5   | r-x       | 4+0+1 | Læse og execute rettigheder         |
|   6   | rw-       | 4+2+0 | Læse og skrive rettigheder          |
|   7   | rwx       | 4+2+1 | Læse, skrive og execute rettigheder |

| User | group | Other |
| :--: | :---: | :---: |
|  u   |   g   |   o   |

| Read | Write | Execute |
| :--: | :---: | :-----: |
|  r   |   w   |    e    |
