---
 hide:
#  - footer
---

# Læringslog uge 05 - *Intro dage*

## Emner

Ugens emner er: 
- ..

## Mål for ugen

- ..

### Praktiske mål

**Praktiske opgaver vi har udført**

- ..

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** ..


## Reflektioner over hvad vi har lært

- ..

## Andet