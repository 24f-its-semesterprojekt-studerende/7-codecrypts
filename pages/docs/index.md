---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Semesterprojekt gruppe 07

<img src="images\CodeCrypts_logo.png" 
      alt="GruppeLogo" 
      width="400" 
      height="auto"
      style="display: block; margin: 0 auto">



På denne side findes gruppe 7 CodeCripts dokkumentation, noter, lærings logs samt semester projekt For 2024 forår semesteret.  

På sidens sidebar kan der navigeres til gruppens læringslogs for fagende: 
- System sikkerhed og netværk  
- Kommunikations sikkerhed  

Samt dokumentationen for gruppens semester projekt.  


Semesterprojektet er beskrevet på: [https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/](https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/)
