---
 hide:
#   - footer
---

# Opsætning af MySql Database 

## Information

Her beskrives de nødvendige trin for at installere og konfigurere en MySQL-database på en Ubuntu-server. Den angiver de grundlæggende procedurer for at oprette og administrere databasen, som støtter webserverens backend-funktionaliteter.

## Instruktioner

Installation af MySQL på Ubuntu-serveren initieres ved at udføre følgende kommando:

"sudo apt install mysql-server"

Denne handling installerer MySQL-serverprogrammet og dens nødvendige komponenter.

Første Autentifikation
Efter installationen skal man autentificere som 'root'-bruger. Ved friske installationer er der ingen forudindstillet adgangskode, hvilket gør det muligt at logge ind uden adgangskode. Når man er logget ind, vises en MySQL-prompt med "mysql" indikeret, hvilket bekræfter, at autentifikationen er succesfuld.


![Første autentifikation](../images/Database_Tutorial_Images/Log_ind.png)



Databaseoprettelse
Herefter oprætter vi ny database ved navn "webserver_db". 


![Oprettelse af database](../images/Database_Tutorial_Images/Create_Database.png)


Bekræftelse af eksisterende databaser kan opnås ved at udføre følgende kommando: "SHOW DATABASES;"


![Eksisterende databaser](../images/Database_Tutorial_Images/Existing_databases.png)



Nu er databasen oprettet. Det næste bliver at åbne databasen så man kan lave et table og tilføje noget data til det via. "USE" kommandoen. Man kan også bruge "SELECT DATABASE();"  


![Åben database](../images/Database_Tutorial_Images/Enter_Database.png)




Når databasen er oprettet og valgt, skal man oprette en tabel til at lagre brugerdata. Dette gøres ved at anvende en SQL-kommando til at skabe en tabel som i dette tildfælde hedder "users" med specifikke kolonner for brugeridentifikation og sikkerhedsinformation. Tilføjelse af data til denne tabel sker ved at indføre rækker, som indeholder brugerens id, brugernavn og adgangskode.


![Opret table](../images/Database_Tutorial_Images/Create_table.png)


![Insættelse af data](../images/Database_Tutorial_Images/Insert_into_table.png)


Dataudtrækning
For at validere og se alt indholdet i 'users'-tabellen, kan følgende kommando benyttes:


![Visning af data](../images/Database_Tutorial_Images/Select_Table.png)


Denne kommando henter og viser alle rækker fra 'users'-tabellen, hvilket muliggør en verifikation af de indsatte data.


## Links

https://dev.mysql.com/doc/mysql-getting-started/en/#mysql-getting-started-basic-ops

