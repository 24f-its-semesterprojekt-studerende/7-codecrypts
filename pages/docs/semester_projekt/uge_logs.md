# Ugelog

## Uge 1

- Opsætning af rapport
- Opsætning af Kanbanboard
- Problemstilling
- Problemfomulering
- Opsætning af metode afsnit
- Rapport skrivning

## Uge 2

- Opsætning af test miljø
- Opsætning af Apache webserver
  - Opsætning af login page på webserver
- Opsætning af mySQL database
- Sammenkobling mellem Database og webserver
- Troubleshooting af kontakt mellem database og webserver
- Testing af CWE319 om cleartext transmission
- Rapport skrivning

## Uge 3

- Opsætning af Wazuh Server
- Opsætning af Wazuh Agent
- Opsætning af integrations tjek på databasen
- Test af sikre miljø
- Rapportskrivning

## Uge 4

- Rapportskrivning
