# Wazuh Agent Opsætning

Først intastets Wasuh serverens lokale adresse i browseren  
![s1](../images/Wazuh_Agent_Tutorial/Screenshot_1.png)

Klik på pilen øverst for at folde menuen ud  
![s2](../images/Wazuh_Agent_Tutorial/Screenshot_2.png)

Klik Agents  
![s3](../images/Wazuh_Agent_Tutorial/Screenshot_3.png)

Nederst på siden klik deploy agents  
![s4](../images/Wazuh_Agent_Tutorial/Screenshot_4.png)

Select DEB amd64  
![s5](../images/Wazuh_Agent_Tutorial/Screenshot_5.png)

Scroll ned, server adressen er den samme som den i url'en  
![s6](../images/Wazuh_Agent_Tutorial/Screenshot_6.png)

Kopier komandoerne og kør dem på den ønskede maskine hvor agenten skal sættes op  
![s7](../images/Wazuh_Agent_Tutorial/Screenshot_7.png)

Enable agenten med disse kommandoer på maskinen  
![s8](../images/Wazuh_Agent_Tutorial/Screenshot_8.png)

I dette tilfælde benyttes der ssh fra en Kali enhed for at kopire komandoerne  
![s9](../images/Wazuh_Agent_Tutorial/Screenshot_9.png)

![s10](../images/Wazuh_Agent_Tutorial/Screenshot_10.png)

![s11](../images/Wazuh_Agent_Tutorial/Screenshot_11.png)

Hvis agenten er sat op korrekt, så kan den nu ses under Agents på Wazuh dashboarded  
![s12](../images/Wazuh_Agent_Tutorial/Screenshot_12.png)