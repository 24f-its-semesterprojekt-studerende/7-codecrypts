# Opsætning af Wazuh

## Opsætning af Wazuh Logging på Apache Webserver
Der vil blive opsamlet logs fra databasen, samt fra Apache WebServeren, så det bliver centraliseret i Wazuh Server. 

For at opsamle specifikke logs skal agenten konfigureres til at overvåge relevante logfiler. Dette gøres inde i ossec.conf filen. Den ligger i /var/ossec/etc/ossec.conf 
Først skal ”logcollector” aktiveres for at kunne overvåge logfiler. 
Længere nede i konfigurationen skal der angives hvilken log der skal overvåges.

![Log collector](../images/Wazuh/log_collector.png)

``<logcollector> Section:`` 

``<scan_on_start>yes</scan_on_start>: Denne indstilling angiver, at Log Collector skal foretage en scanning ved opstart af agenten. Dette sikrer, at ingen vigtige logfiler går ubemærket hen lige fra systemstart.``

``<enabled>yes</enabled>: Aktiverer logopsamlingsmodulet. Dette er afgørende for at Wazuh-agenten kan indsamle logdata.``

``<directories>/usr/bin,/usr/sbin</directories>: Angiver stierne til de mapper, hvor logopsamling skal foretages. Dette er typisk steder, hvor system- og applikationsværktøjer ligger, og der kan genereres logdata.``

![Log format](../images/Wazuh/logging_file.png)

<log_format>syslog</log_format>: 

Dette angiver formatet på de logfiler, der skal overvåges. I dette tilfælde er det angivet til "syslog". Det betyder, at Wazuh forventer, at logdataene vil være i syslog-format. 

<location>/var/log/flaskapp/flaskapp_login_attempts.log</location>: 

Dette angiver den præcise sti til logfilen, som Wazuh skal overvåge. I dette tilfælde er det en logfil, der sandsynligvis indeholder optegnelser over loginforsøg til en Flask-applikation. Ved at overvåge denne fil kan man opfange og analysere uautoriserede eller mislykkede loginforsøg, hvilket er afgørende for sikkerhedsovervågning.

For at wazuh agenten kan overvåge filen er det vigtigt at den bruger som agenten arbejder ud fra, har rettighederne til at læse filen.  
 
Kommandoen for at angive ejerskabs rettighedder for en specifik fil på en specifik bruger er: ”sudo chown wazuh /var/log/flaskapp/flaskapp_login_attempts.log” 

Log-filen “flaskapp_login_attempts.log” bliver genereret i back-enden af hjemmesiden i et python script, “pyApp.py”.

![Flask app logging](../images/Wazuh/logging_flaskApp.png)

“logging.basicConfig” er en opsætnings funktion der angiver en standard på hvordan loggen skal skrives, hvis andet ikke er angivet.  
filename: Angiver filen, hvor logmeddelelserne vil blive skrevet. I dette tilfælde er det /var/log/flaskapp/flaskapp_login_attempts.log. 


level: Sætter logniveauet til INFO, hvilket betyder, at kun meddelelser med sværhedsgrad INFO eller højere vil blive logget. Niveauerne er DEBUG, INFO, WARNING, ERROR, og CRITICAL. 

format: Angiver formatet på logmeddelelserne. Formatet her er '%(asctime)s - %(message)s', hvor: 

%(asctime)s inkluderer dato og tid for logmeddelelsen. 

%(message)s inkluderer selve logmeddelelsen. 

datefmt: Angiver formatet på dato og tid. Formatet her er '%Y-%m-%d %H:%M:%S', hvilket vil vise dato og tid som År-Måned-Dag Time:Minut:Sekund.

Efter “logging.basicConfig” er sat op kan logging.info og logging.warning trækkes med den ønskede log-tekst, og en log-tekst med dato, username og hvorvidt den givne bruger var i stand til at autentificere sig.

![alt text](../images/Wazuh/login_logging.png)

## Opsætning af Wazuh Logging på Databasen

På databasen er der en agent som hver tiende sekund laver en SHA-256 check-sum på hele databasen, og ligger et op mod den tidligere returnede hash.

![alt text](../images/Wazuh/db_intregrity.png)

``<disabled>no</disabled>: Dette indikerer, at Syscheck er aktiveret. Hvis det stod til "yes", ville funktionen være deaktiveret.``

``<frequency>10</frequency>: Definerer, hvor ofte (i sekunder) Syscheck skal gentage sine integritetskontroller. Her er det sat til hver 10. sekund, hvilket betyder, at Syscheck kontrollerer integriteten af de specificerede filer hver 10. sekund.``

``<scan_on_start>yes</scan_on_start>: Dette betyder, at Syscheck vil udføre en fuld integritetskontrol, når Wazuh-agenten starter. Dette sikrer, at ingen ændringer, der er sket, mens systemet var nedlukket eller da agenten ikke kørte, går ubemærket hen.``

``<directories check_all="yes">/var/lib/mysql/webserver_db/users.ibd</directories>: Angiver den præcise sti til den fil eller mappe, som Syscheck skal overvåge. Attributtet check_all="yes" betyder, at alle ændringer til denne fil registreres og kontrolleres for integritet.``